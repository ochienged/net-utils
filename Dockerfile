FROM alpine:latest

RUN apk update \
 && apk add wget curl tcpdump netcat-openbsd openssh-client tmux git \
 && rm -rf /var/cache/apk/*
